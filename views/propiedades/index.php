<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Propiedades';
?>
<div class="propiedades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar propiedad nueva', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'poblacion',
            [
                'attribute' => 'Dirección completa',
                'value' => 'dir_completa',
                
            ],
            [
                'attribute' => 'Número de habitaciones',
                'value' => 'num_habitaciones',
                
            ],
            [
                'attribute' => 'ascensor',
                'value' => function($model) {
                    return $model->ascensor == 1 ? 'Sí' : 'No';
                }
            ],
            'id_propietario',
            [
                'attribute' => 'teléfono del propietario',
                'value' => 'propietario.tlf',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
