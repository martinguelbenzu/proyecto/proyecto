<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = $titulo;
?>
<div class="col-lg-9">
    <h1><?= $titulo ?>:</h1>
</div>
<div class="col-lg-3" >
    <h2>Buscar en otras ciudades:</h2>
</div>
<div class="row">
    <div class="col-lg-9">
        <div class="thumbnail">
            <div class="caption">
                <?= GridView::widget(['dataProvider' => $resultado, 'columns' => $campos]); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3" >
        <nav>
            <ul style="margin-top: 40px ">
                <li><?= Html::a('Barcelona', ['propiedades/consultahabitaciones','pob' => 'Barcelona'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Bilbao', ['propiedades/consultahabitaciones','pob' => 'Bilbao'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Cordoba', ['propiedades/consultahabitaciones','pob' => 'Cordoba'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Granada', ['propiedades/consultahabitaciones','pob' => 'Granada'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Madrid', ['propiedades/consultahabitaciones','pob' => 'Madrid'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Malaga', ['propiedades/consultahabitacione','pob' => 'Malaga'], ['class' => 'btn btn-primary']) ?></li> 
            </ul>
            <ul>
                <li><?= Html::a('Oviedo', ['propiedades/consultahabitaciones','pob' => 'Oviedo'], ['class' => 'btn btn-primary']) ?></li> 
            </ul>
            <ul>
                <li><?= Html::a('Pamplona', ['propiedades/consultahabitaciones','pob' => 'Pamplona'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Salamanca', ['propiedades/consultahabitaciones','pob' => 'Salamanca'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Santander', ['propiedades/consultahabitaciones','pob' => 'Santander'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Segovia', ['propiedades/consultahabitaciones','pob' => 'Segovia'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Sevilla', ['propiedades/consultahabitaciones','pob' => 'Sevilla'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Valencia', ['propiedades/consultahabitaciones','pob' => 'Valencia'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Valladolid', ['propiedades/consultahabitaciones','pob' => 'Valladolid'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Vigo', ['propiedades/consultahabitaciones','pob' => 'Vigo'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
            <ul>
                <li><?= Html::a('Vitoria', ['propiedades/consultahabitaciones','pob' => 'Vitoria'], ['class' => 'btn btn-primary']) ?></li>
            </ul>
        </nav>
    </div>
</div>

