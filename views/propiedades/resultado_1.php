<?php

use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = $titulo;
?>
<div class="col-lg-9">
    <h1><?= $titulo ?>:</h1>
</div>
<div class="col-lg-3" >
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="thumbnail">
            <div class="caption">
                <?= GridView::widget(['dataProvider' => $resultado, 'columns' => $campos]); ?>
            </div>
        </div>
    </div>
</div>