<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Propiedades */

$this->title = 'id_propiedad-' . $model->id_propiedad;
$this->params['breadcrumbs'][] = ['label' => 'Propiedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="propiedades-view">

    <h1>Propiedad <?= Html::encode($model->id_propiedad) ?>:  <?= Html::encode($model->dir_completa) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_propiedad], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id_propiedad], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_propiedad',
            'poblacion',
            [
                'attribute'=>'Dirección',
                'value'=>$model->dir_completa,
            ],
            [
                'attribute'=>'Número de habitaciones',
                'value'=>$model->num_habitaciones,
            ],
            [
                'attribute' => 'ascensor',
                'value' => function($model) {
                    return $model->ascensor == 1 ? 'Sí' : 'No';
                }
            ],
            'id_propietario',
        ],
    ]) ?>

</div>
