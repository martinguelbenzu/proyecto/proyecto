<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Propiedades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="propiedades-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'poblacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dir_completa')->textInput(['maxlength' => true]) -> label('Dirección completa')?>

    <?= $form->field($model, 'num_habitaciones')->textInput() -> label('Número de habitaciones')?>

    <?= $form->field($model, 'ascensor')->textInput() ->label('Ascensor: Si=1 / No=0')?>

    <?= $form->field($model, 'id_propietario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
