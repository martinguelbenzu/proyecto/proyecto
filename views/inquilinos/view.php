<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inquilinos */

$this->title = $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Inquilinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->dni;
\yii\web\YiiAsset::register($this);
?>
<div class="inquilinos-view">

    <h1>DNI: <?= Html::encode($model->dni) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_inquilino], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id_inquilino], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_inquilino',
            'dni',
            [
                'attribute'=>'teléfono del cliente',
                'value'=>$model->tlf,
            ],
            [
                'attribute'=>'Fecha de nacimiento',
                'value'=>$model->f_nac,
            ],
        ],
    ]) ?>

</div>
