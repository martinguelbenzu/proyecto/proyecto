<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inquilinos';
?>
<div class="inquilinos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrate como inquilino después de haber acordado el alojamiento con el cliente de la propiedad (id_cliente requerido)', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id_inquilino',
            'dni',
            [
                'attribute'=>'teléfono del inquilino',
                'value'=>'tlf',
            ],
            [
                'attribute'=>'Fecha de nacimiento',
                'value'=>'f_nac',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
