<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inquilinos */

$this->title = 'Actualizar información del inquilino: id_' . $model->id_inquilino . '  -  DNI ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Inquilinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_inquilino, 'url' => ['view', 'id' => $model->id_inquilino]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="inquilinos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
