<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'UniClean - Portal para estudiantes';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>¡Bienvenido a UniClean!</h1>

        <p class="lead">Visita nuestro listado completo de propiedades.</p>        
    </div>



    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">                
                <h2>Consultar propiedades en las que alojarte donde estudias:</h2>
                <div class="col-lg-3">
                    <nav>
                        <ul style="margin-top: 20px ">
                            <li><?= Html::a('Barcelona', ['propiedades/consultahabitaciones', 'pob' => 'Barcelona'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Bilbao', ['propiedades/consultahabitaciones', 'pob' => 'Bilbao'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Cordoba', ['propiedades/consultahabitaciones', 'pob' => 'Cordoba'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Granada', ['propiedades/consultahabitaciones', 'pob' => 'Granada'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Madrid', ['propiedades/consultahabitaciones', 'pob' => 'Madrid'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Malaga', ['propiedades/consultahabitaciones', 'pob' => 'Malaga'], ['class' => 'btn btn-primary']) ?></li> 
                        </ul>
                        <ul>
                            <li><?= Html::a('Oviedo', ['propiedades/consultahabitaciones', 'pob' => 'Oviedo'], ['class' => 'btn btn-primary']) ?></li> 
                        </ul>
                        <ul>
                            <li><?= Html::a('Pamplona', ['propiedades/consultahabitaciones', 'pob' => 'Pamplona'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <nav>
                        <ul style="margin-top: 20px ">
                            <li><?= Html::a('Salamanca', ['propiedades/consultahabitaciones', 'pob' => 'Salamanca'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Santander', ['propiedades/consultahabitaciones', 'pob' => 'Santander'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Segovia', ['propiedades/consultahabitaciones', 'pob' => 'Segovia'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Sevilla', ['propiedades/consultahabitaciones', 'pob' => 'Sevilla'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Valencia', ['propiedades/consultahabitaciones', 'pob' => 'Valencia'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Valladolid', ['propiedades/consultahabitaciones', 'pob' => 'Valladolid'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Vigo', ['propiedades/consultahabitaciones', 'pob' => 'Vigo'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <ul>
                            <li><?= Html::a('Vitoria', ['propiedades/consultahabitaciones', 'pob' => 'Vitoria'], ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="col-lg-6">
                <h2>Consultar la mensualidad correspondiente a cada propiedad:</h2>
                <div style="margin-top: 20px ">
                    <div style="text-align: center"><?= Html::a('Consultar',['propiedades/consultaprecios'], ['class' => 'btn btn-primary']) ?></div>
                </div>
            </div>
            <div class="col-lg-6">
                <hr>
                <hr>
                <h2>UniClean</h2>
                <hr>
                <p>El portal inmobiliario en el que encontrarás la propiedad ideal para compartirla con tus amig@s, compañer@s, o conocer a tus compañer@s de piso ideales.</p>
                <p>Consulta la lista completa de <?= Html::a('propiedades', ['propiedades/index'], ['class' => 'link1']) ?>, distintos usuarios con sus datos de contacto, ya sean <?= Html::a('Propietarios', ['propietarios/index'], ['class' => 'link2']) ?>, <?= Html::a('Clientes', ['clientes/index'], ['class' => 'link3']) ?> o <?= Html::a('Inquilinos', ['inquilinos/index'], ['class' => 'link4']) ?>.</p>
                <p>Quien quiera poner una propiedad en alquiler debe registrarse primero como propietario para que se pueda contactar con el/ella. </p>
                <p>Si alguien firma el contrato de una propiedad registrada, deberá registrarse como cliente con el Id de la propiedad. </p>
                <p>Quien se aloje como inquilino deberá inscribirse como inquilino con el Id del cliente que le aloje en la tabla <?= Html::a('alojamientos', ['alojamientos/index'], ['class' => 'link5']) ?>. </p>
                <p>Todas las <?= Html::a('limpiezas', ['limpiezas/index'], ['class' => 'link6']) ?> serán registradas una vez se soliciten, se responsabiliza el cliente, por eso es necesaria su Id.</p>
            </div>


        </div>
    </div>

