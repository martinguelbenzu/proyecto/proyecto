<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tlf')->textInput(['maxlength' => true]) ->label('Teléfono') ?>

    <?= $form->field($model, 'f_nac')->textInput() ->label('Fecha de nacimiento')?>

    <?= $form->field($model, 'precio_firmado')->textInput() ?>

    <?= $form->field($model, 'id_propiedad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
