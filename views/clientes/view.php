<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->dni;
\yii\web\YiiAsset::register($this);
?>
<div class="clientes-view">

    <h1>DNI: <?= Html::encode($model->dni) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_cliente], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id_cliente], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_cliente',
            'dni',
            [
                'attribute'=>'teléfono del cliente',
                'value'=>$model->tlf,
            ],
            [
                'attribute'=>'Fecha de nacimiento',
                'value'=>$model->f_nac,
            ],
            'precio_firmado',
            'id_propiedad',
        ],
    ]) ?>

</div>
