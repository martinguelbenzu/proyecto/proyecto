<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
?>
<div class="clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar firmante de alquiler', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id_cliente',
            'dni',
            [
                'attribute'=>'teléfono del cliente',
                'value'=>'tlf',
            ],
            [
                'attribute'=>'Fecha de nacimiento',
                'value'=>'f_nac',
            ],
            'precio_firmado',
            'id_propiedad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
