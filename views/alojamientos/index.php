<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alojamientos';
?>
<div class="alojamientos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir alojamiento', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id_alojamiento',
            'id_cliente',
            [
                'attribute'=>'teléfono del cliente',
                'value'=>'cliente.tlf',
            ],
            [
                'attribute'=>'DNI del cliente',
                'value'=>'cliente.dni',
            ],
            
            'id_inquilino',
            [
                'attribute'=>'teléfono del inquilino',
                'value'=>'inquilino.tlf',
            ],
            [
                'attribute'=>'DNI del inquilino',
                'value'=>'inquilino.dni',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
