<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alojamientos */

$this->title = 'Actualizar Alojamiento: ' . $model->id_alojamiento;
$this->params['breadcrumbs'][] = ['label' => 'Alojamientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_alojamiento, 'url' => ['view', 'id' => $model->id_alojamiento]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="alojamientos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
