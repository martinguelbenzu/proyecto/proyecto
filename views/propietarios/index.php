<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Propietarios';
?>
<div class="propietarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrarte como Propietario (Necesario antes de registrar tu propiedad)', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dni',
            [
                'attribute'=>'teléfono del propietario',
                'value'=>'tlf',
            ],
            'e_mail',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
