<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Limpiezas';
?>
<div class="limpiezas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir limpieza', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id_limpieza',
            'coste',
            [
                'attribute'=>'general',
                'value'=>function($model) {return $model->general == 1 ? 'Sí' : 'No';}
            ],
            'fecha',
            'id_cliente',
            [
                'attribute'=>'teléfono del cliente',
                'value'=>'cliente.tlf',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
