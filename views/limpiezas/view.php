<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Limpiezas */

$this->title = $model->id_limpieza;
$this->params['breadcrumbs'][] = ['label' => 'Limpiezas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="limpiezas-view">

    <h1>Limpieza <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_limpieza], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id_limpieza], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_limpieza',
            'coste',
            [
                'attribute'=>'general',
                'value'=>function($model) {return $model->general == 1 ? 'Sí' : 'No';}
            ],
            'fecha',
            'id_cliente',
        ],
    ]) ?>

</div>
