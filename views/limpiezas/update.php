<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Limpiezas */

$this->title = 'Actualizar limpieza código: ' . $model->id_limpieza;
$this->params['breadcrumbs'][] = ['label' => 'Limpiezas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_limpieza, 'url' => ['view', 'id' => $model->id_limpieza]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="limpiezas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
