<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Limpiezas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="limpiezas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'coste')->textInput() ?>

    <?= $form->field($model, 'general')->textInput() ->label('General=1 / Habitación=0') ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'id_cliente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar'
                . '', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
