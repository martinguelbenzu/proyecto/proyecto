<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propietarios".
 *
 * @property int $id_propietario
 * @property string $dni
 * @property string $tlf
 * @property string $e_mail
 *
 * @property Propiedades[] $propiedades
 */
class Propietarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propietarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf', 'e_mail'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['tlf'], 'string', 'max' => 11],
            [['e_mail'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_propietario' => 'Id Propietario',
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'e_mail' => 'E Mail',
        ];
    }

    /**
     * Gets query for [[Propiedades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropiedades()
    {
        return $this->hasMany(Propiedades::className(), ['id_propietario' => 'id_propietario']);
    }
}
