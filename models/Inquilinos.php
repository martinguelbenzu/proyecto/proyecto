<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inquilinos".
 *
 * @property int $id_inquilino
 * @property string $dni
 * @property string $tlf
 * @property string $f_nac
 *
 * @property Alojamientos[] $alojamientos
 */
class Inquilinos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inquilinos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf', 'f_nac'], 'required'],
            [['f_nac'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['tlf'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_inquilino' => 'Id Inquilino',
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'f_nac' => 'F Nac',
        ];
    }

    /**
     * Gets query for [[Alojamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlojamientos()
    {
        return $this->hasMany(Alojamientos::className(), ['id_inquilino' => 'id_inquilino']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->f_nac=Yii::$app->formatter->asDate($this->f_nac, 'php:d-m-Y');
    }
}
