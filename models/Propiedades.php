<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propiedades".
 *
 * @property int $id_propiedad
 * @property string $poblacion
 * @property string $dir_completa
 * @property int $num_habitaciones
 * @property int $ascensor
 * @property int $id_propietario
 *
 * @property Clientes $clientes
 * @property Propietarios $propietario
 */
class Propiedades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propiedades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poblacion', 'dir_completa', 'num_habitaciones', 'ascensor', 'id_propietario'], 'required'],
            [['num_habitaciones', 'ascensor', 'id_propietario'], 'integer'],
            [['poblacion', 'dir_completa'], 'string', 'max' => 50],
            [['id_propietario'], 'exist', 'skipOnError' => true, 'targetClass' => Propietarios::className(), 'targetAttribute' => ['id_propietario' => 'id_propietario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_propiedad' => 'Id Propiedad',
            'poblacion' => 'Poblacion',
            'dir_completa' => 'Dir Completa',
            'num_habitaciones' => 'Num Habitaciones',
            'ascensor' => 'Ascensor',
            'id_propietario' => 'Id Propietario',
        ];
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasOne(Clientes::className(), ['id_propiedad' => 'id_propiedad']);
    }

    /**
     * Gets query for [[Propietario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario()
    {
        return $this->hasOne(Propietarios::className(), ['id_propietario' => 'id_propietario']);
    }
}
