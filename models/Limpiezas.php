<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "limpiezas".
 *
 * @property int $id_limpieza
 * @property float|null $coste
 * @property int|null $general
 * @property string $fecha
 * @property int $id_cliente
 *
 * @property Clientes $cliente
 */
class Limpiezas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'limpiezas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coste'], 'number'],
            [['general', 'id_cliente'], 'integer'],
            [['fecha', 'id_cliente'], 'required'],
            [['fecha'], 'safe'],
            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['id_cliente' => 'id_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_limpieza' => 'Id Limpieza',
            'coste' => 'Coste',
            'general' => 'General',
            'fecha' => 'Fecha',
            'id_cliente' => 'Id Cliente',
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['id_cliente' => 'id_cliente']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
    }
}
