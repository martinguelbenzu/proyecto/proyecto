<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id_cliente
 * @property string $dni
 * @property string $tlf
 * @property string $f_nac
 * @property float $precio_firmado
 * @property int $id_propiedad
 *
 * @property Alojamientos[] $alojamientos
 * @property Propiedades $propiedad
 * @property Limpiezas[] $limpiezas
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'tlf', 'f_nac', 'precio_firmado', 'id_propiedad'], 'required'],
            [['f_nac'], 'safe'],
            [['precio_firmado'], 'number'],
            [['id_propiedad'], 'integer'],
            [['dni'], 'string', 'max' => 9],
            [['tlf'], 'string', 'max' => 11],
            [['id_propiedad'], 'unique'],
            [['id_propiedad'], 'exist', 'skipOnError' => true, 'targetClass' => Propiedades::className(), 'targetAttribute' => ['id_propiedad' => 'id_propiedad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cliente' => 'Id Cliente',
            'dni' => 'Dni',
            'tlf' => 'Tlf',
            'f_nac' => 'F Nac',
            'precio_firmado' => 'Precio Firmado',
            'id_propiedad' => 'Id Propiedad',
        ];
    }

    /**
     * Gets query for [[Alojamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlojamientos()
    {
        return $this->hasMany(Alojamientos::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * Gets query for [[Propiedad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropiedad()
    {
        return $this->hasOne(Propiedades::className(), ['id_propiedad' => 'id_propiedad']);
    }

    /**
     * Gets query for [[Limpiezas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLimpiezas()
    {
        return $this->hasMany(Limpiezas::className(), ['id_cliente' => 'id_cliente']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->f_nac=Yii::$app->formatter->asDate($this->f_nac, 'php:d-m-Y');
    }
}
