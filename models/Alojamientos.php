<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alojamientos".
 *
 * @property int $id_alojamiento
 * @property int $id_cliente
 * @property int $id_inquilino
 *
 * @property Clientes $cliente
 * @property Inquilinos $inquilino
 */
class Alojamientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alojamientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cliente', 'id_inquilino'], 'required'],
            [['id_cliente', 'id_inquilino'], 'integer'],
            [['id_cliente', 'id_inquilino'], 'unique', 'targetAttribute' => ['id_cliente', 'id_inquilino']],
            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['id_cliente' => 'id_cliente']],
            [['id_inquilino'], 'exist', 'skipOnError' => true, 'targetClass' => Inquilinos::className(), 'targetAttribute' => ['id_inquilino' => 'id_inquilino']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_alojamiento' => 'Id Alojamiento',
            'id_cliente' => 'Id Cliente',
            'id_inquilino' => 'Id Inquilino',
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * Gets query for [[Inquilino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInquilino()
    {
        return $this->hasOne(Inquilinos::className(), ['id_inquilino' => 'id_inquilino']);
    }
}
