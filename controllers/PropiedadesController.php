<?php

namespace app\controllers;

use Yii;
use app\models\Propiedades;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * PropiedadesController implements the CRUD actions for Propiedades model.
 */
class PropiedadesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Propiedades models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Propiedades::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Propiedades model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Propiedades model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Propiedades();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_propiedad]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Propiedades model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_propiedad]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Propiedades model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Propiedades model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Propiedades the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Propiedades::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionConsultahabitaciones($pob)
    {
        $numero = Yii::$app->db
            ->createCommand("SELECT COUNT(*) FROM (SELECT * FROM(
  SELECT p.id_propiedad, p.poblacion poblacion, p.dir_completa AS Direccion_Completa, c.id_cliente, p.num_habitaciones,COUNT(a.id_inquilino)+1 AS habitaciones_ocupadas, (p.num_habitaciones-(COUNT(a.id_inquilino)+1)) AS HABITACIONES_LIBRES
  FROM clientes c
    INNER JOIN alojamientos a ON c.id_cliente = a.id_cliente
    INNER JOIN propiedades p ON c.id_propiedad = p.id_propiedad
      GROUP BY c.id_cliente
  )hab_libres WHERE hab_libres.poblacion = '$pob' ) AS C2 ")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT * FROM(
  SELECT p.id_propiedad, p.poblacion poblacion, p.dir_completa AS Direccion_Completa, c.id_cliente, p.num_habitaciones,COUNT(a.id_inquilino)+1 AS habitaciones_ocupadas, (p.num_habitaciones-(COUNT(a.id_inquilino)+1)) AS HABITACIONES_LIBRES, c.tlf AS Numero_de_contacto
  FROM clientes c
    INNER JOIN alojamientos a ON c.id_cliente = a.id_cliente
    INNER JOIN propiedades p ON c.id_propiedad = p.id_propiedad
      GROUP BY c.id_cliente
  )hab_libres WHERE hab_libres.poblacion = '$pob' AND hab_libres.HABITACIONES_LIBRES>0",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 20,
            ]
           
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Direccion_Completa','id_cliente','num_habitaciones','habitaciones_ocupadas','HABITACIONES_LIBRES', 'Numero_de_contacto'],
            "titulo"=>"Propiedades disponibles en $pob",
        ]);
    }
    
    public function actionConsultaprecios()
    {
        $numero1 = Yii::$app->db
            ->createCommand("SELECT COUNT(*)
   FROM (SELECT p.id_propiedad, c.precio_firmado, SUM(l.coste) AS total_limpieza,FORMAT ( ( (SUM(l.coste) + c.precio_firmado) / (COUNT(a.id_inquilino)+1) ), 2) AS Precio_por_persona, COUNT(a.id_cliente)+1 AS HABITACIONES_OCUPADAS
  FROM clientes c 
    INNER JOIN alojamientos a ON c.id_cliente = a.id_cliente 
    INNER JOIN propiedades p ON c.id_propiedad = p.id_propiedad
    INNER JOIN limpiezas l ON c.id_cliente = l.id_cliente
      GROUP BY c.id_cliente
        ORDER BY p.id_propiedad ) c1")
            ->queryScalar();
        $dataProvider1 = new SqlDataProvider([
            'db' => Yii::$app->db,
            'sql'=>"SELECT C1.id_propiedad,C1.precio_firmado,SUM(l.coste) AS total_limpieza,FORMAT( (C1.Precio_por_persona+(SUM(l.coste)/C1.HABITACIONES_OCUPADAS)),2) AS Precio_por_persona, C1.HABITACIONES_OCUPADAS
  FROM (SELECT c.id_cliente, p.id_propiedad, c.precio_firmado, FORMAT ( ( c.precio_firmado / (COUNT(a.id_inquilino)+1) ), 2) AS Precio_por_persona, COUNT(a.id_cliente)+1 AS HABITACIONES_OCUPADAS
  FROM clientes c 
    INNER JOIN alojamientos a ON c.id_cliente = a.id_cliente 
    INNER JOIN propiedades p ON c.id_propiedad = p.id_propiedad
      GROUP BY c.id_cliente
        ORDER BY p.id_propiedad) AS C1
    INNER JOIN limpiezas l ON l.id_cliente=C1.id_cliente
      GROUP BY C1.id_cliente
        ORDER BY C1.id_propiedad",
            'totalCount'=>$numero1,
            'pagination'=>[
                'pageSize' => 20,
            ]
        ]);
        return $this->render("resultado_1",[
            "resultado"=>$dataProvider1,
            "campos"=>['id_propiedad','precio_firmado','Precio_por_persona','total_limpieza','HABITACIONES_OCUPADAS'],
            "titulo"=>"Precio del alquiler por persona",
        ]);
    }
}